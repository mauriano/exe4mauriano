#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::atualizar()
{
    QString busca = ui->busca->text();
    QString texto = ui->texto->toPlainText();

    bool sensitive = !ui->ignora->isChecked();

    texto.replace(busca, QString("<font color=\"#FF0000\">" + busca + "</font>")
                  , sensitive ? Qt::CaseSensitive : Qt::CaseInsensitive);

    ui->texto->setText(texto);
}

void MainWindow::on_busca_textChanged(const QString &arg1)
{
    atualizar();
}

void MainWindow::on_ignora_stateChanged(int arg1)
{
    atualizar();
}
